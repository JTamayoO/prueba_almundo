# Prueba Front-End Empresa Almundo
El exámen está compuesto por dos aplicaciones:
- Backend escrito en NodeJS el cual expondrá una API REST para la interacción con la aplicación frontend.
- Front End: Maquetar una página de resultado de hoteles, ver imágenes en el repo (solo mobile y desktop). Consumir la API REST, implementando las funcionalidades necesarias para listar y filtar los hoteles.

# Aspectos Importantes.

- Se usó AngularJs, base de Datos MongoDb y Redux para permitir mayor control del estado de la aplicación, crear un producto de manera consistente.
- Las funcionalidades construidas permiten el listado, filtrado de hoteles, se tuvieron en cuenta factores como escalabilidad, reutilización y separación de responsabilidades.
- Se realizó la lógica para tener paginación, filtrado por nombre, estrellas y precio.
- Se realizó un diseño responsive con boostrap (grid), y se usó flexbox para mejorar los estilos en todos los viewports.
- La implentación fue aplicando la técnica Mobile First.
- Se implementaron pruebas, tareas automatizadas con Gulp, hay sincronización constante de los archivos y los tests para generar la carpeta dist lista para producción.
- Se implementó ES6 por lo tanto se configuro babel. 
- Se implementó iconos por medio de render como fuente para optimización.
- Se realizó el proceso de optimización de recursos para producción. 
- Para uso adecuado se debe primero importar el archivo JSON en la base de datos MongoDB.
- Se usó NPM para el manejo de dependencias.
- Se añadio persistencia de Datos con Mongo y se incluyeron en la Api los métodos de Actualizar y Eliminar.
- Se usó SASS para el CSS.

# Para Correr la Api Rest 
- Instalar y Correr MongoDB: https://docs.mongodb.com/manual/tutorial/install-mongodb-on-os-x/ 
- Ejecutar mongoimport --db hotels --collection hotels --file "/api-rest/data/data.json" --jsonArray
- Luego de tener la base de datos importada, ir a la carpeta "/api-rest"
- Usar el comando: npm install
- Usar el comando: npm start
- Abrir el navegador en http://localhost:3002/api/hotels

# Para Correr la Página.
- Correr el primero la Api Rest.
- Usar el comando: npm install
- Usar el comando: npm run serve
- Abrir el Navegador en http://localhost:3001/
