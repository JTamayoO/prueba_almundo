import Hotel from '../models/hotel'

class HotelController {

  constructor() {
  }

  create(req, res) {
    const newHotel = new Hotel(req.body);
    newHotel.save((err, hotel) => {
      if (err) {
        res.send(err);
      }
      res.json(hotel);
    });
  }
  
  get(req, res) {
    Hotel.findOne({_id: req.params.id}, function(err, result) {
      if (err) {
        return res.send(err);
      }
      return res.json(result);
    });
  }

  createQuery(query) {
    let newQuery=[];

    if (query.name){
      newQuery.push({name : new RegExp(query.name.toLowerCase(), 'i')});
    }
    
    if (query.star){
      newQuery.push({stars : { $in: query.star.split(',').map(Number) }});
    }

    if (query.price_range){
      let range = query.price_range.split('-').map(Number);
      newQuery.push({"price" : { $gt : range[0], $lt: range[1]}});
    }

    return newQuery.length > 0 ?{$and: newQuery} :{};
  }

  search(req, res) {
    let query = this.createQuery(req.query);
    let options = this.getOptions(req);

    Hotel.paginate(query, options, function(err, result) {
      if (err) {
        res.statusCode = 404;
        return res.json({ errors: ['Could not retrieve hotels'] });
      }
      return res.json(result);
    });
  }

  getOptions(req) {
    let offset = parseInt(req.query.offset);
    let limit = parseInt(req.query.limit);

    if (Number.isNaN(offset) || offset < 1) {
      offset = 0;
    }

    if (Number.isNaN(limit)) {
      limit = 5;
    } else if (limit > 50) {
      limit = 50;
    } else if (limit < 1) {
      limit = 5;
    }
    
    return { limit, offset }
  }

  update(req, res) {
    Hotel.update(req.params.id, req.body, function(err, result) {
      if (err) {
        return res.send(err);
      }
      return res.json(results);
    });
  }

  delete(req, res) {
    Hotel.remove({_id: req.params.id}, function(err, result) {
      if (err) {
        return res.send(err);
      }
      return res.json(results);
    });
  }
}
export default new HotelController();
