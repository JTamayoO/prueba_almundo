import template from './header.template.html'
import controller from './header.controller'

export const HeaderComponent = {
  template,
  controller
}
