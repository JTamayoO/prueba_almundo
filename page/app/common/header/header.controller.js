
class HeaderController {
  constructor($ngRedux, $scope, URL_LOGO) {
    this.routeHotelImages = URL_LOGO;
  }
}

HeaderController.$inject = ['$ngRedux', '$scope', 'URL_LOGO'];

export default HeaderController;
