
import template from './hotel-item.template.html';
import controller from './hotel-item.controller'

export const HotelItemComponent = {
  bindings: {
    hotel: '<'
  },
  controller,
  template
}
