
class HotelItemController {
  constructor($ngRedux, $scope, URL_IMAGES, URL_ICONS) {
    this.routeHotelImages = URL_IMAGES;
    this.routeAmenitiesIcons = URL_ICONS;
  }
}

HotelItemController.$inject = ['$ngRedux', '$scope', 'URL_IMAGES', 'URL_ICONS'];

export default HotelItemController;
