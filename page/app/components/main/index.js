/**
 * components/hotels/index.js
 *
 * Module that contains the hotels components and service
 * and configure the routed component for
 * / Hotels path route.
 */

import angular from 'angular'

import { HotelItemComponent } from './hotels/hotel-item/hotel-item.component'
import { HotelsComponent } from './hotels/hotels.component'
import { MainComponent } from './main.component'

import filters from './filters'
import HotelActions from '../../actions/hotel.actions'
import HotelServices from '../../services/hotel.services';

const main = angular
  .module('app.main', [filters])
  .constant('URL_API','http://localhost:3002/api/hotels')
  .constant('URL_IMAGES', "http://localhost:3002/images/hotels/")
  .constant("URL_ICONS", "http://localhost:3002/icons/amenities/")
  .constant("URL_LOGO", "http://localhost:3002/images/")
  .factory('HotelActions', HotelActions)
  .service('HotelServices', HotelServices)
  .component('main', MainComponent)
  .component('hotelItem', HotelItemComponent)
  .component('hotelList', HotelsComponent)
  .config(($stateProvider, $urlRouterProvider) => {
    $stateProvider
      .state('main', {
        url: '/:city',
        component: 'main'
      });
    $urlRouterProvider.otherwise('/')
  })
  .name

export default main
